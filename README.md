# Allock

A stack implementation in C specialized for emulating C++ destructors.

## Why is this useful?

It might not be. I made it mostly to see if I could, but also because I felt like it would help out with destructor emulation with the use of `goto`.

Let's say you're writing a function that needs to allocate a number of resources and performs operations with them. If it fails anywhere along the way, you need to clean up all the resources allocated so far. You also want to return a different negative integer for each kind of failure.

Here's a way to write one such function by using early return statements.

```c
char *mystring = malloc(16);
if (mystring == NULL)
{
  return -1;
}
strcpy(mystring, "test");

FILE *fp = fopen("testfile", "w");
if (fp == NULL)
{
  free(mystring);
  return -2;
}

int written = fprintf(fp, mystring);
if (written == -1)
{
  free(mystring);
  fclose(fp);
  return -3;
}

/* More resource allocation and such */

return 0;
```

See how early return statements can stack up? This becomes much more relevant when you need to be managing/propagating return codes.

Nesting can also look nice when things don't get too deep, but as the number of resources increases, so does the tabulation.

```c
int result = 0;
char *mystring = malloc(16);
if (mystring != NULL)
{
  strcpy(mystring, "test");

  FILE *fp = fopen("testfile", "w");
  if (fp != NULL)
  {
    int written = fprintf(fp, mystring);
    if (written != -1)
    {
      /* More resource allocation and such */
    }
    else
    {
      result = -3;
    }
    fclose(fp);
  }
  else
  {
    result = -2;
  }
  free(mystring);
}
else
{
  result = -1
}

return result;
```

But an alternative is to use `goto`.

```c
int result = 0;
char *mystring = NULL;
FILE *fp = NULL;

mystring = malloc(16);
if (mystring == NULL)
{
  result = -1;
  goto done;
}
strcpy(mystring, "test");

fp = fopen("testfile", "w");
if (fp == NULL)
{
  result = -2;
  goto done;
}

int written = fprintf(fp, mystring);
if (written == -1)
{
  result = -3;
  goto done;
}

/* More resource allocation and such */

done:
free(mystring);
fclose(fp);
return result;
```

The above is pretty similar to how most of the Linux kernel is written. The use of `goto` is argued to be more readable and maintainable when it comes to cleaning up allocated resources. This is particularly important in the Kernel because resources allocated in Kernel mode NEVER GET FREED unless explicitly called for. That is, if an application running in Kernel mode is terminated, it WILL NOT FREE THINGS AUTOMATICALLY. If you really care about cleaning up after yourself in C code at scale, this can be a good practice. It ultimately depends on the balance between readability and maintainability.

As a supplement to the above, we introduce the `allock_stack`. You can register destructors for resources as you go and clean them up in one foul swoop at the end.

```c
int result = 0;
char *mystring = NULL;
FILE *fp = NULL;
allock_stack stack = ALLOCK_STACK_INIT;

mystring = malloc(16);
allock_push(&stack, mystring, free);
if (mystring == NULL)
{
  result = -1;
  goto done;
}
strcpy(mystring, "test");

fp = fopen("testfile", "w");
allock_push(&stack, fp, fclose);
if (fp == NULL)
{
  result = -2;
  goto done;
}

int written = fprintf(fp, mystring);
if (written == -1)
{
  result = -3;
  goto done;
}

/* More resource allocation and such */

done:
allock_stack_free(&stack);
return result;
```

In fact, if you don't need to access any of them after the `done` label, you don't need to declare everything at the top.

```c
int result = 0;
allock_stack stack = ALLOCK_STACK_INIT;

char *mystring = malloc(16);
allock_push(&stack, mystring, free);
if (mystring == NULL)
{
  result = -1;
  goto done;
}
strcpy(mystring, "test");

FILE *fp = fopen("testfile", "w");
allock_push(&stack, fp, fclose);
if (fp == NULL)
{
  result = -2;
  goto done;
}

int written = fprintf(fp, mystring);
if (written == -1)
{
  result = -3;
  goto done;
}

/* More resource allocation and such */

done:
allock_stack_free(&stack);
return result;
```

# Usage

Just plop `allock.c` and `allock.h` into your project and you're set to go!

