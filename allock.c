#include <stdlib.h>
#include <string.h>
#include "allock.h"

#ifndef ALLOCK_ASSERTIONS
#define ALLOCK_ASSERTIONS 0
#endif
#if ALLOCK_ASSERTIONS
#include <signal.h>
#define allock_assert(expression) \
  if (!(expression))              \
  {                               \
    raise(SIGABRT);               \
  }
#else
#define allock_assert(expression) ((void)0)
#endif

void allock_entry_free(const allock_entry entry)
{
  if (entry.destroy != NULL)
  {
    entry.destroy(entry.handle);
  }
}

// Windows has Data Execution Prevention (DEP) that outlaws executing an address in stack memory.
// Since an allock_entry may be allocated on the stack, we need a way to bypass DEP.
// Our weapon of choice is to copy it to the heap before doing any executions.
// https://learn.microsoft.com/en-us/previous-versions/windows/it-pro/windows-xp/bb457155(v=technet.10)#data-execution-prevention
static allock_entry* heap_helper = NULL;
static size_t heap_helper_count = 0;

void allock_entry_free_all_depsafe(const allock_entry *entries, size_t count)
{
  if (heap_helper == NULL)
  {
    heap_helper = ALLOCK_MALLOC(sizeof(allock_entry) * count);
    allock_assert(heap_helper);
    heap_helper_count = count;
  }
  else if (heap_helper_count < count)
  {
    allock_entry* new_helper = ALLOCK_REALLOC(heap_helper, sizeof(allock_entry) * count);
    allock_assert(new_helper);
    if (new_helper)
    {
      heap_helper = new_helper;
      heap_helper_count = count;
    }
  }
  memcpy(heap_helper, entries, sizeof(allock_entry) * count);
  size_t i;
  for (i = 0; i < count; i++)
  {
    allock_entry entry = heap_helper[i];
    allock_entry_free(entry);
  }
}

void allock_entry_free_all(const allock_entry *entries, size_t count)
{
  size_t i;
  for (i = 0; i < count; i++)
  {
    allock_entry entry = entries[i];
    allock_entry_free(entry);
  }
}

void allock_stack_reuse(allock_stack *stack)
{
  allock_assert(stack != NULL);
  switch (stack->flags)
  {
  case ALLOCK_USING_ALLOCA:
  case ALLOCK_USING_MIXED_ALLOCA:
    {
      memset(stack->stack, 0, stack->capacity);
      stack->count = 0;
    }
    break;
  case ALLOCK_USING_MALLOC:
  case ALLOCK_USING_MIXED_MALLOC:
    {
      allock_assert(stack->stack == NULL);
      char original_flags = stack->flags;
      if (stack->capacity == 0)
        stack->capacity = ALLOCK_DEFAULT_CAPACITY;
      *stack = ALLOCK_STACK_INIT_MALLOC(stack->capacity);
      stack->flags = original_flags;
    }
    break;
  default:
    allock_assert(0);
  }
}

int allock_stack_push(allock_stack *stack, allock_entry entry)
{
  allock_assert(stack->count <= stack->capacity);
  if (stack->count == stack->capacity)
  {
    allock_assert(stack->flags != ALLOCK_USING_ALLOCA);
    if (stack->flags == ALLOCK_USING_ALLOCA)
    {
      return -1;
    }
    size_t new_capacity = stack->capacity * 2;
    allock_entry *new_stack = NULL;
    if (stack->flags == ALLOCK_USING_MIXED_ALLOCA)
    {
      new_stack = ALLOCK_MALLOC(sizeof(allock_stack) * new_capacity);
      if (new_stack)
      {
        stack->flags = ALLOCK_USING_MIXED_MALLOC;
      }
    }
    else
    {
      allock_assert(stack->flags == ALLOCK_USING_MALLOC || stack->flags == ALLOCK_USING_MIXED_MALLOC);
      new_stack = ALLOCK_REALLOC(stack->stack, sizeof(allock_stack) * new_capacity);
    }
    if (new_stack == NULL)
    {
      return -1;
    }
    stack->capacity = new_capacity;
    stack->stack = new_stack;
  }
  memcpy(&stack->stack[stack->count], &entry, sizeof(entry));
  ++stack->count;
  return (int)stack->count;
}

allock_entry allock_stack_peek(allock_stack *stack)
{
  if (stack->count == 0)
  {
    return ALLOCK_NULL;
  }
  return stack->stack[stack->count - 1];
}

int allock_stack_pop(allock_stack *stack)
{
  if (stack->count == 0)
  {
    return -1;
  }
  --stack->count;
  memset(&stack->stack[stack->count], 0, sizeof(allock_entry));
  return (int)stack->count;
}

void allock_stack_free(allock_stack *stack)
{
  if (stack->flags == ALLOCK_USING_ALLOCA || stack->flags == ALLOCK_USING_MIXED_ALLOCA)
  {
    allock_entry_free_all_depsafe(stack->stack, stack->count);
  }
  else
  {
    allock_entry_free_all(stack->stack, stack->count);
  }
  if (stack->flags == ALLOCK_USING_MALLOC || stack->flags == ALLOCK_USING_MIXED_MALLOC)
  {
    ALLOCK_FREE(stack->stack);
    stack->stack = NULL;
    stack->capacity = 0;
  }
  stack->count = 0;
}

int allock_push(allock_stack *stack, void *resource, allock_func destructor)
{
  allock_entry entry = {resource, destructor};
  return allock_stack_push(stack, entry);
}

int allock_pop(allock_stack *stack)
{
  allock_entry entry = allock_stack_peek(stack);
  int result = allock_stack_pop(stack);
  if (stack->flags == ALLOCK_USING_ALLOCA || stack->flags == ALLOCK_USING_MIXED_ALLOCA)
  {
    if (heap_helper == NULL)
    {
      heap_helper = ALLOCK_MALLOC(sizeof(allock_entry));
      allock_assert(heap_helper);
      heap_helper_count = 1;
    }
    memcpy(heap_helper, &entry, sizeof(entry));
    memcpy(&entry, heap_helper, sizeof(entry));
  }
  allock_entry_free(entry);
  return result;
}
