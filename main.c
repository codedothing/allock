#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#define ALLOCK_DEBUG
// #define ALLOCK_DEFAULT_ALLOCATION_STRATEGY 1
#include "allock.h"

static size_t alloc_count = 0;

void *mallock(size_t nbytes)
{
  void *ret = malloc(nbytes);
  if (ret)
  {
    ++alloc_count;
  }
  return ret;
}

void freek(void *mem)
{
  --alloc_count;
  free(mem);
}

/* This is sorta an example of how I intend to use this */
#if 0
bool dosomething()
{
  thing *mything = NULL;
  thing2 *myotherthing = NULL;
  FILE *fd = NULL;
  allock_stack stack = ALLOCK_STACK_INIT;
  bool result = false;

  mything = libthing_create();
  allock_push(&stack, mything, libthing_destroy);
  if (!mything)
  {
    goto done;
  }

  myotherthing = libthing_createother();
  allock_push(&stack, myotherthing, libthing_destroyother);
  if (!myotherthing)
  {
    goto done;
  }

  /* .... */

  fd = fopen("myfile", "w");
  allock_push(&stack, fd, fclose);
  if (!fd)
  {
    goto done;
  }

  /* ... */

done:
  allock_stack_free(&stack);
  return result;
}
#endif

/* main */
int main(int argc, char **argv)
{
  int result = 0;
  /* Test initializer */
  allock_stack mystack = ALLOCK_STACK_INIT;
  assert(mystack.count == 0);
  assert(mystack.capacity > 0);
  assert(mystack.stack != NULL);

  /* Test isnull */
  allock_entry nullentry = allock_stack_peek(&mystack);
  assert(allock_isnull(nullentry));

  allock_entry notnullentry = {malloc(1), free};
  assert(!allock_isnull(notnullentry));

  /* Test push */
  result = allock_stack_push(&mystack, nullentry);
  assert(mystack.count == 1);
  assert(result == mystack.count);

  result = allock_stack_push(&mystack, notnullentry);
  assert(mystack.count == 2);
  assert(result == mystack.count);

  allock_log("peeking");
  allock_entry peekedentry = allock_stack_peek(&mystack);
  assert(!allock_isnull(peekedentry));

  allock_log("popping");
  result = allock_stack_pop(&mystack);
  assert(mystack.count == 1);
  assert(result == mystack.count);

  allock_log("peeking again");
  allock_entry peekedentry2 = allock_stack_peek(&mystack);
  assert(allock_isnull(peekedentry2));

  /* Test free */
  allock_stack_free(&mystack);
  assert(mystack.count == 0);
  assert(mystack.stack == NULL && mystack.capacity == 0 || mystack.capacity == 16);
  assert(mystack.stack == NULL || mystack.capacity == 16);

  /* Test init */
  allock_stack_reuse(&mystack);
  assert(mystack.count == 0);
  assert(mystack.capacity > 0);
  assert(mystack.stack != NULL);

  /* Test alloc/free count */
  size_t expected_count = 4;
  size_t i;
  for (i = 0; i < expected_count; i++)
  {
    assert(alloc_count == i);
    result = allock_push(&mystack, mallock(16), freek);
    assert(result == mystack.count);
    allock_log("Expected count: %zu", i + 1);
    allock_log("Actual count: %zu", alloc_count);
    assert(alloc_count == (i + 1));
  }
  assert(alloc_count == expected_count);
  for (i = expected_count; i > 0; i--)
  {
    assert(alloc_count == i);
    result = allock_pop(&mystack);
    assert(result == mystack.count);
    allock_log("Expected count: %zu", i - 1);
    allock_log("Actual count: %zu", alloc_count);
    assert(alloc_count == (i - 1));
  }
  assert(alloc_count == 0);

  /* This won't crash - I hope */
  result = allock_pop(&mystack);
  assert(result == -1);

  /* hey this is failing wtf */
  FILE *fp = fopen("./somefile", "w");
  allock_log("Error? %s", strerror(errno));
  assert(fp != NULL);
  allock_push(&mystack, (void *)fp, (void *)fclose);

  int written = fprintf(fp, "test");
  assert(written == 4);
  allock_stack_free(&mystack);
  errno = 0;
  written = fprintf(fp, "test");
  allock_log("Error? %s", strerror(errno));
  assert(errno != 0);
  assert(written == -1);

  return 0;
}
