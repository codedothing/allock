#ifndef ALLOCK_H
#define ALLOCK_H

// You may choose to override the default stack allocator.
#ifndef ALLOCK_ALLOCA
// Windows
#if defined _WIN32
#include <malloc.h> // alloca
#define ALLOCK_ALLOCA _alloca
// MacOS
#elif defined __APPLE__
#include <alloca.h> // alloca
#define ALLOCK_ALLOCA alloca
// Linux
#elif defined __unix__
#include <alloca.h> // alloca
#define ALLOCK_ALLOCA alloca
#else
#error "Unknown platform! Please define ALLOCK_ALLOCA to continue!"
#endif
#endif

// You may choose to override the default heap allocator.
// If you do, be sure that allock.c has been recompiled with the same definitions.
#ifndef ALLOCK_MALLOC
#include <stdlib.h> // malloc, realloc, free
#define ALLOCK_MALLOC malloc
#define ALLOCK_REALLOC realloc
#define ALLOCK_FREE free
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifdef ALLOCK_DEBUG
#include <stdio.h>
#define allock_log(...) do { printf(__VA_ARGS__); printf("\n"); fflush(stdout); } while(0)
#endif

/**
 * Basically a 'free' signature.
 */
typedef void (*allock_func)(void*);

/**
 * A generic resource/destructor pair.
 */
typedef struct allock_entry {
  void              *handle;
  const allock_func  destroy;
} allock_entry;

/**
 * A "NULL" object that can go on the stack (but shouldn't).
 * Used as a "NULL" output on some functions.
 */
const allock_entry ALLOCK_NULL;

/**
 * Check if the input is a "NULL" object.
 * @param[in] entry The entry to check.
 */
#define allock_isnull(entry) (((entry).handle == 0) && ((entry).destroy == 0))

/**
 * A stack for resource/destructor pairs.
 */
typedef struct allock_stack {
  size_t        count;
  size_t        capacity;
  allock_entry *stack;
  char          flags;
} allock_stack;

#ifndef ALLOCK_DEFAULT_CAPACITY
#define ALLOCK_DEFAULT_CAPACITY 16
#endif
#define ALLOCK_USING_MALLOC 1 // Always use malloc
#define ALLOCK_USING_ALLOCA 2 // Always use alloca (unsafe, only good for light use)
#define ALLOCK_USING_MIXED_ALLOCA 3 // Use alloca, then switch to malloc when it runs out of space
#define ALLOCK_USING_MIXED_MALLOC 4 // Started out as ALLOCK_USING_MIXED_ALLOCA, but has since been reallocated on the heap
#ifndef ALLOCK_DEFAULT_ALLOCATION_STRATEGY
#define ALLOCK_DEFAULT_ALLOCATION_STRATEGY ALLOCK_USING_MIXED_ALLOCA
#endif
 
/**
 * Initializers. Used for assignment to allock_stack variables.
 */
#define ALLOCK_STACK_INIT_INTERNAL(size, allocator, strategy) (allock_stack){0, size, allocator(sizeof(allock_entry) * (size)), strategy}
#define ALLOCK_STACK_INIT_MALLOC(size) ALLOCK_STACK_INIT_INTERNAL(size, ALLOCK_MALLOC, ALLOCK_USING_MALLOC)
#define ALLOCK_STACK_INIT_ALLOCA(size) ALLOCK_STACK_INIT_INTERNAL(size, ALLOCK_ALLOCA, ALLOCK_USING_ALLOCA)
#define ALLOCK_STACK_INIT_MIXED(size) ALLOCK_STACK_INIT_INTERNAL(size, ALLOCK_ALLOCA, ALLOCK_USING_MIXED_ALLOCA)
 
#if ALLOCK_DEFAULT_ALLOCATION_STRATEGY == ALLOCK_USING_MALLOC
#define ALLOCK_STACK_INIT_DEFAULT(size) ALLOCK_STACK_INIT_MALLOC(size)
#elif ALLOCK_DEFAULT_ALLOCATION_STRATEGY == ALLOCK_USING_ALLOCA
#define ALLOCK_STACK_INIT_DEFAULT(size) ALLOCK_STACK_INIT_ALLOCA(size)
#elif ALLOCK_DEFAULT_ALLOCATION_STRATEGY == ALLOCK_USING_MIXED_ALLOCA
#define ALLOCK_STACK_INIT_DEFAULT(size) ALLOCK_STACK_INIT_MIXED(size)
#else
#error "Invalid allocation strategy! Please select 1 (always malloc), 2 (always alloca), or 3 (mixed)"
#endif

#define ALLOCK_STACK_INIT ALLOCK_STACK_INIT_DEFAULT(ALLOCK_DEFAULT_CAPACITY)

/**
 * Call the destructor on the entry.
 * @param[in] entry The allocated resource/destructor bundle to "free".
 */
void allock_entry_free(const allock_entry entry);

/**
 * Free all entries in a list. Does not free the list.
 * @param[in] entries The list of entries to free.
 * @param[in] count The number of entries to free.
 */
void allock_entry_free_all(const allock_entry *entries, size_t count);

/**
 * Initialize a stack.
 * Passing NULL will abort the program. Passing a stack that has not been freed by @ref allock_stack_free will also abort the program.
 * @param[in] stack The stack memory to initialize.
 */
void allock_stack_reuse(allock_stack *stack);

/**
 * Push a entry onto the stack.
 * If the stack has been tampered with in such a way that the count has exceeded capacity, the program will abort.
 * @param[in] stack Stack to push onto.
 * @param[in] entry The entry to push onto the stack.
 * @return How many entries are on the stack after the push is complete, or -1 if it fails.
 */
int allock_stack_push(allock_stack *stack, allock_entry entry);

/**
 * Peek at the top entry on the stack.
 * @param[in] stack Stack to peek on.
 * @return allock_entry
 */
allock_entry allock_stack_peek(allock_stack *stack);

/**
 * Pop the top entry from stack.
 * @param[in] stack The stack to pop from.
 * @return How many entries are left on the stack after the pop is complete, or -1 if the stack is already empty.
 */
int allock_stack_pop(allock_stack *stack);

/**
 * Free the stack and call all destructors for the entries that are on it.
 * @param[in] stack The stack to free.
 */
void allock_stack_free(allock_stack *stack);

/**
 * Push a resource/destructor pair onto the stack.
 * @param[in] stack The stack to push to.
 * @param[in] resource The resource to manage.
 * @param[in] destructor The function that releases the resource. It should be of a form compatible with `void func(void *resource)`. This means functions such as fclose could be used.
 * @return How many entries are on the stack after the push is complete, or -1 if it fails.
 */
int allock_push(allock_stack *stack, void *resource, allock_func destructor);

/**
 * Pop and free a resource/destructor pair from the stack.
 * @param[in] stack The stack to pop from.
 * @return How many entries are left on the stack after the pop is complete, or -1 if the stack is already empty.
 */
int allock_pop(allock_stack *stack);

#ifdef __cplusplus
}
#endif
#endif